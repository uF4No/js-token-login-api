/**
 * /api/routes.js
 * exports an express router.
 */ 


const express = require('express');
const router = express.Router();

//to validate tokens
const checkAuth = require('./middleware/check-auth');
//database
const mongoose = require('mongoose');
//import User
const User = require('./user');
//encrypt
const bcrypt = require('bcrypt');
//to generate JWT tokens
const jwt = require('jsonwebtoken');

router.get('/', (req, res) => {
  res.status(200).json({
    message: 'Yabadabadooo'
  });
});

router.post('/register', (req, res, next) => {
  let hasErrors = false ;
  let errors = [];
  
  if(!req.body.name){
  //validate name presence in the request
    errors.push({'name': 'Name not received'})
    hasErrors = true;
  }
  if(!req.body.email){
    //validate email presence in the request
    errors.push({'email': 'Email not received'})
    hasErrors = true;
  }
  if(!req.body.password){
    //validate password presence in the request
    errors.push({'password': 'Password not received'})
    hasErrors = true;
  }

  if(hasErrors){
    //if there is missing field
    res.status(422).json({
      message: "Invalid input",
      errors: errors
    });

  }else{
    //check if mail already exists
    User.findOne({'email': req.body.email}).then((doc, err) => {
      if(doc){
        //if email already exists in DB, send error
        errors.push({email: 'Email already registered'});
        res.status(422).json({
          message: "Invalid email",
          errors: errors
        })
      }else{
        //if no validation errors and mail not in use
        //encrypt user pass
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if(err){
            errors.push({
              hash: err.message
            })
            return res.status(500).json(errors);
          }else{
          //create the user with encrypted pass 
            const new_user = new User({
              _id : mongoose.Types.ObjectId(),
              name: req.body.name,
              email: req.body.email,
              password: hash
              });
            //save in DB
            new_user.save().then(doc => {
              res.status(201).json({
                message: 'User created!',
                errors: errors,
                password: hash
              });
            }).catch(err => {
              console.log(err);
              errors.push(new Error({
                db: err.message
              }))
              res.status(500).json(errors);
            });
          }

        });
      }
    });
     

  }

});

router.post('/login', (req, res, next) => {
  let hasErrors = false ;
  let errors = [];
  
  if(!req.body.email){
    errors.push({'email': 'Email not received'})
    hasErrors = true;
  }
  if(!req.body.password){
    errors.push({'password': 'Password not received'})
    hasErrors = true;
  }

  if(hasErrors){
  //return error code an info
    res.status(422).json({
      message: "Invalid input",
      errors: errors
    });

  }else{
    //find User in DB
    User.findOne({'email': req.body.email}).then((doc, err) => {

      if(!doc){
        //return error, user is not registered
        res.status(401).json({
          message: "Auth error, email not found"
        });

      }else{
        //validate password
        bcrypt.compare(req.body.password, doc.password, (err, valid) => {
          if(err){
            //if it fails validating password, return error
            res.status(500).json({
              message: err.message
            }) 
          }
          if(!valid){
            //return error, incorrect password
            res.status(401).json({
              message: "Auth error"
            }) 
          }else{
            //generate JWT token. Sign receives payload, key and opts.
            const token = jwt.sign({
              email: doc.email, 
              id: doc._id,
              name: doc.name
            }, process.env.JWT_KEY, {
              expiresIn: "1h"
            })
            //validation OK
            res.status(200).json({
              message: 'Auth OK',
              token: token,
              errors: errors
            })
          }
        })

      }
    })

  }

});

router.get('/protected', checkAuth, (req, res, next)=> {
  res.status(200).json({
    message: 'Welcome ' + req.userData.name,
    user: req.userData,
    errors: [],
  })
})


module.exports = router;