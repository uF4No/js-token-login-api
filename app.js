/**
 * api/app.js
 * exports an express app started.
 */

const express = require('express')
const app = express();

//for logs
const morgan = require('morgan');

//to handle requests
const bodyParser = require('body-parser');

//database connection

const mongoose = require('mongoose');

//Database connection
let db_path = "mongodb://" + process.env.DB_USER + ":" + process.env.DB_PASS + "@" + process.env.DB_HOST;
console.log(db_path);
//if(db_path.indexOf('undefined')){
  //db_path = 'mongodb://authSys:secret@127.0.0.1:27017/authSys'
//}

mongoose.connect(db_path, {useNewUrlParser: true }).then( () => {
  console.log('Connected to the database');
}).catch((err) => {
  console.log('Error connecting to the database: ' + err);
  process.exit();
});

//mongoose.Promise = global.Promise;
  

//middleware for log traces in the terminal
//app.use(morgan('dev'));

//middleware to parse requests of extended urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
//middleware to parse requests of content-type - application/json
app.use(bodyParser.json())

//import router with endpoints definitions
const routes = require('./api/routes');
//attach router as a middleware
app.use(routes);


//read port from env variable or default 3000
const port = process.env.APP_PORT || 3000;

//start app
app.listen(port, () => {
  console.log("App started! Listening on port " + port);
});

//exports app
module.exports = app;