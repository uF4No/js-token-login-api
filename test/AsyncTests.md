Different ways to code tests for async methods:


## AXIOS

==========================================
it('Test description', () => {
  axios.get('http//').then( response => {
    expect(response).to.be.equal('awdawd');
    
  })
})

/* WRONG: assertions are done before async method finishes.

==========================================
//adding done 
it('Test description', (done) => {
  axios.get('http//').then( response => {
    expect(response).to.be.equal('awdawd');
    done();
  })
})
/* Works OK but we got no error info if assertion fails.

============================================
//adding done and catch

it('Test description', (done) => {
  axios.get('http//').then( response => {
    expect(response).to.be.equal('awdawd');
    done();
  }).catch(err => {
    console.log(err.message);
  })
})
/* Works perfect. No need to manually fail and we get error info
=====================================================
//with async/await
it('Test description', async() => {
  
  const response = await axios.get('http//');
  expect(response).to.be.equal('awdawd');

})

/* Works OK but we got no error info if assertion fails.
======================================================
//with async/await inside a try/catch
it('Test description', async() => {
  try{
    const response = await axios.get('http//');
    expect(response).to.be.equal('awdawd');
  }catch(err){
    console.log(err.message);
  }
})

/* THIS DOES NOT FAIL PROPERLY: Assertion error is captured in catch. We have to force it to fail adding fail() in the catch.

================================================
//async/await inside a try/catch and assertions outside the try
it('Test description', async() => {
  try{
    const response = await axios.get('http//');
  }catch(err){
    console.log(err.message);
  }
  expect(response).to.be.equal('awdawd');

})

/* SHOULDNT WORK. Also no error info if assertion fails.


===========================================================
//async/await inside a try/catch with fail()
it('Test description', async() => {
  try{
    const response = await axios.get('http//');
    expect(response).to.be.equal('awdawd');
  }catch(err){
    console.log(err.message);
    fail();
  }
})
/* Works perfect.


## CHAI-HTTP
Traditional response:
chai.request(app).get('/').end((err, res) => {

})

Response with Promises

chai.request(app).get('/').then((res) => {
  
}).catch( (err) => {
  return Promise.reject(err); 
  OR
  throw err;
})
