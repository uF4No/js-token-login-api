/**
 * test/test.js
 * Basic tests for Auth system API
 */

//load DEV ENV Variables from config file
const env_config = require('./config');
process.env.APP_PORT = env_config.APP_PORT;
process.env.DB_HOST = env_config.DB_HOST;
process.env.DB_USER = env_config.DB_USER;
process.env.DB_PASS = env_config.DB_PASS
process.env.JWT_KEY = env_config.JWT_KEY;

const chai = require('chai');
const expect = chai.expect;
//for http requests to the app
const http = require('chai-http');
chai.use(http);

//start app
const app = require('../app');

const User = require('../api/user')


var mongoose = require('mongoose');
let db_path = "mongodb://" + process.env.DB_USER + ":" + process.env.DB_PASS + "@" + process.env.DB_HOST;


describe('App basics', () => {
  it('Should exists', () => {
    expect(app).to.be.a('function');
  })
  it('GET / should return 200 and message', (done) => {
    //send request to the app
    chai.request(app).get('/')
      .then((res) => {
        //assertions
        //console.log(res.body);
        expect(res).to.have.status(200);
        expect(res.body.message).to.contain('Yabadabadooo');
        done();
    }).catch(err => {
      console.log(err.message);
    })
  });

  it('Returns 404 error for non defined routes', (done) => {
    chai.request(app).get('/unexisting').then((res) => {
      expect(res).to.have.status(404);
      done();
    });
  });
})

describe('Auth API Endpoints', () => {
  before( (done) => {
    //delete all users 
    User.find().deleteMany().then( res => {
      console.log('Users removed');
      done();
    }).catch(err => {
      console.log(err.message);
    });
  });
  
  describe('User registration', () => {
    it('Should return error 422 when fields missing', (done) => {
      //mock invalid user input
      const wrong_input = {
        "name"  : "",
        "email": "notvalidmail",
        "password": ""
      }
  
      //send request to the app
      chai.request(app).post('/register')
        .send(wrong_input)
          .then((res) => { 
            //console.log(res.body);
            //assertions
            expect(res).to.have.status(422);
            expect(res.body.message).to.be.equal("Invalid input");
            expect(res.body.errors.length).to.be.equal(2);
            done();
      }).catch(err => {
        console.log(err.message);
      })
    });
  
  
    it('Should return 201 and confirmation for valid input', (done) => {
      //mock valid user input
      const new_user = {
        "name"  : "John Wick",
        "email": "john@wick.com",
        "password": "secret"
      }
      //send request to the app
      chai.request(app).post('/register')
        .send(new_user)
          .then((res) => {
            //console.log(res.body);
            //assertions
            expect(res).to.have.status(201);
            expect(res.body.message).to.be.equal("User created!");
            expect(res.body.errors.length).to.be.equal(0);
            done();
          }).catch(err => {
            console.log(err.message);
          })
    });
  
    it('Should return error 422 when email already registered', (done) => {
      //user that already exists (added in previous test)
      const new_user = {
        "name"  : "John Wick",
        "email": "john@wick.com",
        "password": "secret"
      }
      //send request to the app
      chai.request(app).post('/register')
      .send(new_user)
        .then((res) => {
          //console.log(res.body);
          //assertions
          expect(res).to.have.status(422);
          expect(res.body.message).to.be.equal("Invalid email");
          expect(res.body.errors.length).to.be.equal(1);
          done();
      }).catch(err => {
        console.log(err.message);
      })
    })
    
  
    it('Should save password encrypted', (done) => {
      //mock valid user input
      const new_user = {
        "name"  : "John Wick",
        "email": "john2@wick.com",
        "password": "secret"
      }
      //send request to the app
      chai.request(app).post('/register')
        .send(new_user)
          .then((res) => {
            //console.log(res.body);
            //assertions
            
            expect(res.body.password).to.not.be.equal("secret");
            done();
          }).catch(err => {
            console.log(err.message);
          })
    })
  
  })


  describe('User login', () => {
    it('should return error 422 for empty password', (done) => {
      //mock invalid user input
      const wrong_input = {
        "email": "notvalidmail",
        "password": ""
      }
      //send request to the app
      chai.request(app).post('/login')
        .send(wrong_input)
          .then((res) => {
            //console.log(res.body);
            //assertions
            expect(res).to.have.status(422);
            expect(res.body.message).to.be.equal("Invalid input");
            expect(res.body.errors.length).to.be.equal(1);
            done();
          }).catch(err => {
            console.log(err.message);
          })
    }); 
    
    it('should return error 401 for invalid credentials', (done) => {
      //mock invalid user input
      const wrong_input = {
        "email": "john@wick.com",
        "password": "invalidPassword"
      }
      //send request to the app
      chai.request(app).post('/login')
        .send(wrong_input)
          .then((res) => {
            //console.log(res.body);
            //assertions
            expect(res).to.have.status(401);
            expect(res.body.message).to.be.equal("Auth error");
            done();
          }).catch(err => {
            console.log(err.message);
          })
    }); 

    it('should return 200 and token for valid credentials', (done) => {
      //mock invalid user input
      const valid_input = {
        "email": "john@wick.com",
        "password": "secret"
      }
      //send request to the app
      chai.request(app).post('/login')
        .send(valid_input)
          .then((res) => {
            //console.log(res.body);
            //assertions
            expect(res).to.have.status(200);
            expect(res.body.token).to.exist;
            expect(res.body.message).to.be.equal("Auth OK");
            expect(res.body.errors.length).to.be.equal(0);
            done();
          }).catch(err => {
            console.log(err.message);
          })
    });
  })  
  describe('Token protected route', () => {
    it('should return error 401 if no valid token provided', (done) => {
      //sed request with no token
      chai.request(app).get('/protected')
        .set('Authentication', '')
        .then(res => {
          expect(res).to.have.status(401);
          expect(res.body.message).to.be.equal('Auth failed');
          done();
        }).catch(err => {
          console.log(err.message);
        });


    })
    it('should return 200 and user details if valid token provided', (done) => {
      //mock login to get token
      const valid_input = {
        "email": "john@wick.com",
        "password": "secret"
      }
      //send login request to the app to receive token
      chai.request(app).post('/login')
        .send(valid_input)
          .then((res) => {
            //add token to next request Authorization headers as Bearer adw3R£$4wF43F3waf4G34fwf3wc232!w1C"3F3VR
            const token = 'Bearer ' + res.body.token;
            chai.request(app).get('/protected')
              .set('Authorization', token)
              .then(res => {
                //assertions
                expect(res).to.have.status(200);
                expect(res.body.message).to.be.equal('Welcome ' + res.body.user.name);
                expect(res.body.user.email).to.exist;
                expect(res.body.errors.length).to.be.equal(0);

                done();
              }).catch(err => {
                console.log(err.message);
              });
          }).catch(err => {
            console.log(err.message);
          });
    })
  })

  after((done) => {
    //stop app server
    console.log('All tests completed, stopping server....')
    process.exit();
    done();
  });

});

